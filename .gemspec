s = Gem::Specification.new 'chan', '0.1.0' do |s|
    s.name        = 'chan'
    s.version     = '0.1.0'
    s.licenses    = ['MIT']
    s.summary     = 'Go channels in ruby'
    s.description = 'Go channels in ruby with some special features'
    s.authors     = ['Jacob McCollum']
    s.email       = 'jacob.r.mccollum@gmail.com'

    s.extensions = %w(ext/chan/extconf.rb)

    # naturally you must include the extension source in the gem

    s.files = %w(
        ext/chan/extconf.rb
        ext/chan/ext.go
        ext/chan/glue.go
        ext/chan/glue.h
        ext/chan/Makefile
        lib/chan.rb
    )
end
