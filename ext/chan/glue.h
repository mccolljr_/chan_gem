// ===========================================
//
// DO NOT EDIT - this file was auto-generated
//
// ===========================================
#include "ruby.h"
/* these stubs are necessary glue to allow Go to build a ruby extension */
extern void Init_chan();
extern VALUE chanListen(VALUE self);
extern VALUE chanCreate(VALUE self);
extern VALUE chanSend(VALUE self, VALUE val);
extern VALUE chanReceive(VALUE self);
extern VALUE chanClose(VALUE self);