package main

// #include "glue.h"
import "C"
import (
	"sync/atomic"
	"unsafe"
)

var chanClass C.VALUE

type chanl chan C.VALUE

var chans = make(map[int32]chanl)
var count int32

//export Init_chan
func Init_chan() {

	chanClass = C.rb_define_class(C.CString("Channel"), C.rb_cObject)

	C.rb_define_attr(chanClass, C.CString("open"), 1, 0)

	C.rb_define_method(chanClass, C.CString("initialize"), void(C.chanCreate), 0)

	C.rb_define_method(chanClass, C.CString("listen"), void(C.chanListen), 0)

	C.rb_define_method(chanClass, C.CString("send"), void(C.chanSend), 1)

	C.rb_define_method(chanClass, C.CString("receive"), void(C.chanReceive), 0)

	C.rb_define_method(chanClass, C.CString("close"), void(C.chanClose), 0)
}

//export chanListen
func chanListen(self C.VALUE) C.VALUE {
	C.rb_need_block()

	v := int32(C.rb_num2int(C.rb_iv_get(self, C.CString("@chanid"))))

	ch, ok := chans[v]

	if !ok {
		return C.Qnil
	}

	for {
		open := C.rb_iv_get(self, C.CString("@open"))

		if open == C.Qtrue {
			select {
			case i := <-ch:
				C.rb_yield(i)
			default:
				C.rb_thread_wait_for(C.rb_time_interval(C.rb_float_new(C.double(0.25))))
			}
		} else {
			if len(ch) > 0 {
				select {
				case i := <-ch:
					C.rb_yield(i)
				default:
					C.rb_thread_wait_for(C.rb_time_interval(C.rb_float_new(C.double(0.25))))
				}
			} else {
				break
			}
		}
	}

	delete(chans, v)

	return C.Qnil
}

//export chanCreate
func chanCreate(self C.VALUE) C.VALUE {
	v := atomic.LoadInt32(&count)
	atomic.AddInt32(&count, 1)

	chans[v] = make(chanl, 100)

	C.rb_iv_set(self, C.CString("@chanid"), C.rb_int2inum(C.long(v)))
	C.rb_iv_set(self, C.CString("@open"), C.Qtrue)

	return C.Qnil
}

//export chanSend
func chanSend(self, val C.VALUE) C.VALUE {
	open := C.rb_iv_get(self, C.CString("@open"))

	if open == C.Qtrue {
		v := int32(C.rb_num2int(C.rb_iv_get(self, C.CString("@chanid"))))

		ch, ok := chans[v]

		if ok {
			// sleep the ruby thread while we wait, otherwise we do bad things
			for len(ch) == cap(ch) {
				C.rb_thread_wait_for(C.rb_time_interval(C.rb_float_new(C.double(0.25))))
			}
			ch <- val
		}

		return C.Qnil
	}

	C.rb_eval_string(C.CString("raise 'channel closed'"))
	return C.Qnil
}

//export chanReceive
func chanReceive(self C.VALUE) C.VALUE {
	open := C.rb_iv_get(self, C.CString("@open"))
	v := int32(C.rb_num2int(C.rb_iv_get(self, C.CString("@chanid"))))

	ch, ok := chans[v]

	if !ok {
		return C.Qnil
	}

	if open == C.Qtrue {
		for {
			select {
			case i := <-ch:
				return i
			default:
				C.rb_thread_wait_for(C.rb_time_interval(C.rb_float_new(C.double(0.25))))
			}
		}
	} else {
		if len(ch) > 0 {
			for {
				select {
				case i := <-ch:
					return i
				default:
					C.rb_thread_wait_for(C.rb_time_interval(C.rb_float_new(C.double(0.25))))
				}
			}
		} else {
			delete(chans, v)
		}
	}

	return C.Qnil
}

//export chanClose
func chanClose(self C.VALUE) C.VALUE {
	open := C.rb_iv_get(self, C.CString("@open"))

	if open == C.Qtrue {
		C.rb_iv_set(self, C.CString("@open"), C.Qfalse)

		v := int32(C.rb_num2int(C.rb_iv_get(self, C.CString("@chanid"))))

		ch, ok := chans[v]

		if ok {
			close(ch)
		}
	}

	return C.Qnil
}

// ignore
func main() {}

func void(i unsafe.Pointer) *[0]byte {
	return (*[0]byte)(i)
}
